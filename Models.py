# SVR
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVR
# Importing the libraries
import numpy as np
from sklearn import metrics
from sklearn import linear_model
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
import pandas as pd
def SVRModel():
    # Importing the dataset
    dataset = pd.read_csv('Position_Salaries.csv')
    X = dataset.iloc[:, 1:2].values
    Y = dataset.iloc[:, 2].values
    sc_X = StandardScaler()
    sc_y = StandardScaler()

    # Feature Scaling
    X = sc_X.fit_transform(X)

    Y = np.reshape(a=Y, newshape=(-1, 1))
    Y = sc_y.fit_transform(Y)

    # Fitting SVR to the dataset
    regressor = SVR()

    regressor.fit(X, Y)
    # Predicting a new result
    y_pred = regressor.predict(6.5)
    y_pred = sc_y.inverse_transform(y_pred)
    print(y_pred)

    # Visualising the SVR results
    plt.scatter(X, Y, color='red')
    plt.plot(X, regressor.predict(X), color='blue')
    plt.title('Truth or Bluff (SVR)')
    plt.xlabel('Position level')
    plt.ylabel('Salary')
    plt.show()

def LinearRegression(X, Y, LabelX, LabelY):
        cls = linear_model.LinearRegression()
        #X = np.expand_dims(X, axis=1)
        Y = np.expand_dims(Y, axis=1)

        X = np.array(X)
        print(X.shape , Y.shape)
        print(type(X) , type(Y))

        cls.fit(X, Y)  # Fit method is used for fitting your training data into the model
        accuracy = cls.score(X, Y)  # calc accuracy for x_train , y_train
        prediction = cls.predict(X)
        plt.scatter(X, Y)
        plt.xlabel(LabelX, fontsize=20)
        plt.ylabel(LabelY, fontsize=20)
        plt.plot(X, prediction, color='red', linewidth=3)
        print('Mean Square Error', metrics.mean_squared_error(Y, prediction))
        print('accuracy', accuracy * 100, '%')
        plt.show()





