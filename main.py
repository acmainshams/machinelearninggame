import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn import preprocessing
from preprocessing import *
from build_data_set import *
from Visualization import *
from ML_Model import *
import FileOperations as fop
from sklearn.preprocessing import  LabelEncoder
import statistics
from statistics import mode
import pickle
import  time
from datetime import  datetime
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
def nan_statistics(Data):
    total_missing = Data.isnull().values.sum()
    print("total number of rows is : {}".format(Data.shape[0]))
    print("total number of missing entries is : {}".format(total_missing))
    print("\n statistics for each feature and number of examples that miss that feature \n")
    print(Data.isnull().sum())
    return

val = input("Enter your Choice 0 (Regression) ,Choice 1 (Classification):")
gen = input("Do you need to generate pickle ? (y(Train_Validate Mode)/n(Test Mode))")

if(val=="0"):
 FILE_NAME = "predicting_mobile_game_success_train_set.csv" # File Name
 TestFile = "samples_mobile_game_success_test_set.csv"# change that also
else:
  FILE_NAME="appstore_games_classification.csv"
  TestFile="samples_predicting_mobile_game_success_test_set_classification.csv"# change that also
Data = fop.ReadData(FILE_NAME)
read=fop.ReadData(TestFile)

def preprocessing(Data):
 # prepare the data
 nan_statistics(Data)
 # filter the data corresponding to our decisions
 Data=Rate(Data,val)
 Data = prepare_Data(Data,val)#All preprocessing
 nan_statistics(Data)

 ##########################Call Functions##############################################
 DataFrame = BuildDataframe(Data, val)  # Hold Numerical Data to Build Numerical Data
 frames = [DataFrame,GenresDataset(Data),lanaguageDataset(Data),PrimaryDataset(Data)] # Build DataSet
 final=pd.concat(frames, axis=1, sort=False)# Concatentate the List
 if(val=="0"):
  #final.to_csv("FinalDATASETRegression.csv")# Save DataSet
  print("Senior 2020 , Regression File saved")
  return final
 else:
  #final.to_csv("FinalDATASETClassification.csv")
  print("Senior 2020 , Classification File saved")
  y = []
  if val == '1':
      for i in range(len(final['Rate'])):
          if (final["Rate"][i].strip() == 'High'):
              y.append('0')
          elif (final["Rate"][i].strip() == 'Intermediate'):
              y.append('1')
          elif (final["Rate"][i].strip() == 'Low'):
              y.append('2')
      final['Rate'] = y  # We Label the y with Most Frequent
      return final


################################Call Preprocessing#########################################################33
final=preprocessing(Data)
testData=preprocessing(read)
###############################################################
def Mapping():
 testList=[]
 for col in testData.columns:
    testList.append(col)
 # iterating the columns
 feature_list=[]
 for col in final.columns:
    feature_list.append(col)
 d = pd.DataFrame(0,index=range(len(read)),columns=feature_list)

 for i in testData:
    for j in feature_list:
        if i==j:
            d[i]=testData[i]
 return d
######################Classifer Normal##############################
if (val=='1'and gen=='y'):# classification
  print("@@@@@@@@@@@@@@@@@@@@Classifer Normal Training mode (old File)@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
  LogisticRegressionClassifer(final,'logsticRegression.pkl')
  DecisionTreClassifer(final,'DecisionTreClassifer.pkl')
  RandomForestClassifer(final,'RandomForestClassifer.pkl')
  Knn(final,'Knn.pkl')
  Addaboost(final,'AddaboostClassifer.pkl')
  ################PCA with classifer###############################3
  print("$$$$$$$$$$$$$$$$$$$$$$$$Classifer PCA Training mode (old File)$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
  pca = PCA(n_components=15)
  FinalPCAX = pca.fit_transform(final.iloc[:, 1:])
  yFinal = final.iloc[:, 0]
  pca1 = np.column_stack((yFinal, FinalPCAX))  # all data concatinate
  LogisticRegressionClassifer(pd.DataFrame(pca1),'logsticRegressionPCA.pkl')
  DecisionTreClassifer(pd.DataFrame(pca1),'DecisionTreClassiferPCA.pkl')
  RandomForestClassifer(pd.DataFrame(pca1),'RandomForestClassiferPCA.pkl')
  Knn(pd.DataFrame(pca1),'KnnPCA.pkl')
  Addaboost(pd.DataFrame(pca1),'AddaboostClassiferPCA.pkl')

#SVMModel(final)

d=Mapping()


if (val=="1"and gen=="n"):
 print("%%%%%%%%%%%%%%%%%%%%%Test mode Classifer Normal (New File) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
 FinalTest('DecisionTreClassifer.pkl',d,"Decision Tree ")
 FinalTest('logsticRegression.pkl',d,"Logistic Regression ")
 FinalTest('Knn.pkl',d,"KNN")
 FinalTest('RandomForestClassifer.pkl',d,"RandomForest ")
 FinalTest("AddaboostClassifer.pkl",d,"Addabost ")
 print("&&&&&&&&&&&&&&&&&&&&Test mode Classifer PCA (New File) &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
 pca = PCA(n_components=15)
 TestDataPCAX =pca.fit_transform(testData.iloc[:,1:])
 yTestDataPCA=testData.iloc[:,0]
 pca2 = np.column_stack((yTestDataPCA, TestDataPCAX))
 FinalTest('logsticRegressionPCA.pkl', pd.DataFrame(pca2), "Logistic Regression ")
 FinalTest('DecisionTreClassiferPCA.pkl', pd.DataFrame(pca2), "Decision Tree ")
 FinalTest('logsticRegressionPCA.pkl', pd.DataFrame(pca2), "Logistic Regression ")
 FinalTest('KnnPCA.pkl',pd.DataFrame(pca2), "KNN")
 FinalTest('RandomForestClassiferPCA.pkl', pd.DataFrame(pca2), "RandomForest ")
 FinalTest("AddaboostClassiferPCA.pkl", pd.DataFrame(pca2), "Addabost ")


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Regression part%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if val=='0' and gen=="y":#train
  print("@@@@@@@@@@@@@@@@@@@@Regression Normal Training mode (old File)@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
  Linear_Regression(final,'SimpleLinear_Regression.pkl')
  SV_regression(final,"sv_reg.pkl")
  print("****************************Regression PCA Training mode (old File)*********************************************************************")

  pca = PCA(n_components=15)
  FinalPCAX = pca.fit_transform(final.iloc[:, 1:])
  yFinal = final.iloc[:, 0]
  pca1 = np.column_stack((yFinal, FinalPCAX))  # all data concatinate
  Linear_Regression(pd.DataFrame(pca1), 'SimpleLinear_RegressionPCA.pkl')
  SV_regression(pd.DataFrame(pca1), "sv_regPCA.pkl")

if val == '0' and gen == "n":
    print("%%%%%%%%%%%%%%%%%%%%%Test mode Regression Normal (New File) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    FinalTest('SimpleLinear_Regression.pkl', d ,"SimpleLinearRegression")
    FinalTest("sv_reg.pkl", d ,"SVMRegression")

    print("%%%%%%%%%%%%%%%%%%%%%Test mode Regression PCA (New File) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    pca = PCA(n_components=15)
    TestDataPCAX = pca.fit_transform(testData.iloc[:, 1:])
    yTestDataPCA = testData.iloc[:, 0]
    pca2 = np.column_stack((yTestDataPCA, TestDataPCAX))
    FinalTest('SimpleLinear_RegressionPCA.pkl', pd.DataFrame(pca2) ,"SimpleLinearRegression ")
    FinalTest("sv_regPCA.pkl", pd.DataFrame(pca2) ,"SVMRegression ")

