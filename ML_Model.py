from datetime import datetime

import numpy as np
import pandas as pd
import sklearn
from sklearn import linear_model
from sklearn.svm import  SVR
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import mean_squared_error
from sklearn.decomposition import PCA
from preprocessing import prepare_Data
import pickle
from sklearn.multiclass import OneVsRestClassifier
from sklearn.model_selection import train_test_split
from sklearn.svm import LinearSVC, SVC
from sklearn.preprocessing import StandardScaler

from sklearn.metrics import r2_score
def Linear_Regression(Data , FileName,poly=False, degree=4 , bias=True):
    X = Data.iloc[: , 1:]
    Y = Data.iloc[:,0]
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, shuffle=True, random_state=0)
    if poly:
        poly = PolynomialFeatures(degree=degree, include_bias=bias)
        X = poly.fit_transform(X)
    reg = linear_model.LinearRegression()
    reg.fit(X_train , Y_train)  # i guess it do the trainig
    Y_pred = reg.predict(X_test)
    print("mean square error is(Linear Regression) {}".format(mean_squared_error(Y_test, Y_pred)))
    pickle.dump(reg, open(FileName, 'wb'))  #####
    print(sklearn.metrics.r2_score(Y_test, Y_pred, sample_weight=None, multioutput='uniform_average'))
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
def confusionMatricx(actual,predicted):
    results = confusion_matrix(actual, predicted)
    print('Confusion Matrix :')
    print(results)
    print('Accuracy Score :', accuracy_score(actual, predicted))
    print('Report : ')
    print(classification_report(actual, predicted))

def SV_regression(Data , FileName,poly=False , degree=2, bias=False):

    X = Data.iloc[: , 1:]
    Y = Data.iloc[: , 0]
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, shuffle=True, random_state=0)
    if poly:
        poly = PolynomialFeatures(degree=degree, include_bias=bias)
        X = poly.fit_transform(X)

    sv_reg = SVR( C=1.0 , epsilon=0.2)
    sv_reg.fit(X_train , Y_train)
    Y_pred = sv_reg.predict(X_test)
    pickle.dump(sv_reg, open(FileName, 'wb'))  #####
    print("mean square error is (SV_regression) {}".format(mean_squared_error(Y_test, Y_pred)))
    print(sklearn.metrics.r2_score(Y_test, Y_pred, sample_weight=None, multioutput='uniform_average'))

def predict_regression(input , reg , prep = False):
    """
    :param input:  supposed to be pandas frame
    :param reg:
    :return:
    """
    if not prep:
        input = prepare_Data(input)

    Avg_user_rate = reg.predict([input])
    print("average user rate is " , Avg_user_rate)
    return Avg_user_rate

def conv_to_poly(X , degree=2 , bias=True):
    poly = PolynomialFeatures(degree=degree, include_bias=bias)
    X = poly.fit_transform(X)
    return X
from sklearn.tree import  DecisionTreeClassifier

def DecisionTreClassifer(Data,pickelName):
   X = Data.iloc[:, 1:]
   Y = Data.iloc[:, 0]


   X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, shuffle=True, random_state=0)
   CurrentTime = time()
   classifer= DecisionTreeClassifier(criterion='entropy',random_state=0)
   classifer.fit(X_train,Y_train)
   EndTime = time()
   print("^^^^^^^^^^^^^^^^^^^^^^^Time Alogrithim^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
   print("DecisionTreClassiferTrain Time", EndTime - CurrentTime)
   CurrentTime = time()#test
   Y_pred = classifer.predict(X_test)
   EndTime = time()#test
   print("DecisionTreClassiferTest Time", EndTime - CurrentTime)#test
   print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
   print("mean square error ( Decision Tree) is {}".format(mean_squared_error(Y_test, Y_pred)))
   print("accuracy ( Decision Tree) is ", classifer.score(X_test, Y_test))
   pickle.dump(classifer, open(pickelName, 'wb'))  #####
   print("@@@@@@@@@@@@@@@@@@@@Confusion Matricx@@@@@@@@@@@@@@@@@@@@@@@@@@")
   confusionMatricx(Y_test,Y_pred)

from timeit import default_timer as time
def LogisticRegressionClassifer(Data,pickelName):
    X = Data.iloc[:, 1:]
    Y = Data.iloc[:, 0]
    #############PCA Option###########33
    #####################
    #sc = StandardScaler()
    #X = sc.fit_transform(X)
    X_train , X_test , Y_train , Y_test = train_test_split(X , Y , test_size=0.2,shuffle=True,random_state=0)
    CurrentTime = time()
    cls = linear_model.LogisticRegression()
    cls.fit(X_train, Y_train)
    EndTime = time()
    print("^^^^^^^^^^^^^^^^^^^^^^^Time Alogrithim^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
    print("LogisticRegressionClassifer Train Time",EndTime - CurrentTime)
    CurrentTime = time()#Test
    Y_pred = cls.predict(X_test)
    EndTime = time()#Test
    print("LogisticRegressionClassifer Test Time",EndTime - CurrentTime)#Test
    print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
    print("mean square error (LogisticRegression) is {}".format(mean_squared_error(Y_test, Y_pred)))
    print("accuracy LogisticRegression is ", cls.score(X_test, Y_test))
    pickle.dump(cls, open(pickelName, 'wb'))  #####
    print("@@@@@@@@@@@@@@@@@@@@Confusion Matricx@@@@@@@@@@@@@@@@@@@@@@@@@@")
    confusionMatricx(Y_test, Y_pred)

def RandomForestClassifer(Data,pickelName):
    X = Data.iloc[:, 1:]
    Y = Data.iloc[:, 0]
    #sc = StandardScaler()
    #X = sc.fit_transform(X)
    #############PCA Option###########33
    #####################
    X_train , X_test , Y_train , Y_test = train_test_split(X , Y , test_size=0.2,shuffle=True,random_state=0)
    # Import Random Forest Model
    from sklearn.ensemble import RandomForestClassifier
    # Create a Gaussian Classifier
    CurrentTime = time()
    clf = RandomForestClassifier(n_estimators=5,criterion='entropy',random_state=0)
    clf.fit(X_train, Y_train)
    EndTime = time()
    print("^^^^^^^^^^^^^^^^^^^^^^^Time Alogrithim^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
    print("RandomForestClassifier Train Time: ",EndTime - CurrentTime)
    CurrentTime = time()#test
    Y_pred = clf.predict(X_test)
    EndTime = time()#Test
    print("RandomForestClassifier Test Time: ",EndTime - CurrentTime)#Test
    print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
    print("mean square (RandomForest) error is {}".format(mean_squared_error(Y_test, Y_pred)))
    print("accuracy (RandomForest) is ", clf.score(X_test, Y_test))
    pickle.dump(clf, open(pickelName, 'wb'))  #####
    print("@@@@@@@@@@@@@@@@@@@@Confusion Matricx@@@@@@@@@@@@@@@@@@@@@@@@@@")
    confusionMatricx(Y_test, Y_pred)

from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
def Addaboost(Data,pickelName):
    X = Data.iloc[:, 1:]
    Y = Data.iloc[:, 0]

    #sc = StandardScaler()
    #X = sc.fit_transform(X)
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, shuffle=True, random_state=0)
    CurrentTime = time()
    bdt = AdaBoostClassifier(DecisionTreeClassifier(max_depth=1),
                             algorithm="SAMME.R",
                             n_estimators=100)

    #scaler = StandardScaler()
    #scaler.fit(X_train)

    #X_train = scaler.transform(X_train)
    #X_test = scaler.transform(X_test)

    bdt.fit(X_train, Y_train)
    EndTime = time()
    print("^^^^^^^^^^^^^^^^^^^^^^^Time Alogrithim^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
    print("Addaboost Train Time",EndTime - CurrentTime)
    CurrentTime = time()#test
    y_prediction = bdt.predict(X_test)
    EndTime = time()#test
    print("Addaboost Test Time",EndTime - CurrentTime)#test
    print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
    accuracy = np.mean(y_prediction == Y_test) * 100
    pickle.dump(bdt, open(pickelName, 'wb'))  #####
    print("The achieved accuracy using Adaboost is " + str(accuracy))
    print("@@@@@@@@@@@@@@@@@@@@Confusion Matricx@@@@@@@@@@@@@@@@@@@@@@@@@@")
    confusionMatricx(Y_test, y_prediction)
def Knn(Data,pickelName):
    X = Data.iloc[:, 1:]
    Y = Data.iloc[:, 0]
    #c = StandardScaler()
    #X = sc.fit_transform(X)
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, shuffle=True, random_state=0)
    from sklearn.neighbors import KNeighborsClassifier
    CurrentTime = time()
    classifier = KNeighborsClassifier(n_neighbors=5)
    classifier.fit(X_train, Y_train)
    EndTime = time()
    print("^^^^^^^^^^^^^^^^^^^^^^^Time Alogrithim^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
    print("KNN Train Time",EndTime - CurrentTime)
    CurrentTime = time()#test
    Y_pred = classifier.predict(X_test)
    EndTime = time()#test
    print("KNN Test Time",EndTime - CurrentTime)
    print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
    print("mean square error (Knn) is {}".format(mean_squared_error(Y_test, Y_pred)))
    print("accuracy (Knn) is ", classifier.score(X_test, Y_test))
    pickle.dump(classifier, open(pickelName, 'wb'))  #####
    print("@@@@@@@@@@@@@@@@@@@@Confusion Matricx@@@@@@@@@@@@@@@@@@@@@@@@@@")
    confusionMatricx(Y_test, Y_pred)


def SVMModel(Data):

    X = Data.iloc[:, 1:]
    Y = Data.iloc[:, 0]
    # dividing X, y into train and test data
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=0, shuffle=True)
    # training a linear SVM classifier
    svm_model_linear_ovr = OneVsRestClassifier(SVC(kernel='linear', C=1)).fit(X_train, y_train)
    pickle.dump(svm_model_linear_ovr, open('modellinear1.pkl', 'wb')) #####
    svm_predictions = svm_model_linear_ovr.predict(X_test)
    print("mean square error is {}".format(mean_squared_error(y_test, svm_predictions)))
    # model accuracy for X_test
    accuracy = svm_model_linear_ovr.score(X_test, y_test)
    print('One VS Rest SVM accuracy: ' + str(accuracy))
    svm_model_linear_ovo = SVC(kernel='linear', C=1).fit(X_train, y_train)

    pickle.dump(svm_model_linear_ovo, open('modellinear2.pkl', 'wb')) #####
    svm_predictions = svm_model_linear_ovo.predict(X_test)
    print("mean square error is {}".format(mean_squared_error(y_test, svm_predictions)))
    # model accuracy for X_test
    accuracy = svm_model_linear_ovo.score(X_test, y_test)
    print('One VS One SVM accuracy: ' + str(accuracy))
def SVMKernelModel(X,Y):

    # dividing X, y into train and test data
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=0, shuffle=True)
    # training a linear SVM classifier
    svm_model_linear_ovr = OneVsRestClassifier(SVC(kernel='poly', C=1, degree=3)).fit(X_train, y_train)
    pickle.dump(svm_model_linear_ovr, open('modelKernal1.pkl', 'wb')) #####
    svm_predictions = svm_model_linear_ovr.predict(X_test)
    print("mean square error is {}".format(mean_squared_error(y_test, svm_predictions)))
    # model accuracy for X_test
    accuracy = svm_model_linear_ovr.score(X_test, y_test)

    print('One VS Rest SVM accuracy Kernal: ' + str(accuracy))
    svm_model_linear_ovo = SVC(kernel='poly', C=1, degree=3).fit(X_train, y_train)
    pickle.dump(svm_model_linear_ovo, open('modelKernal2.pkl', 'wb')) #####
    svm_predictions = svm_model_linear_ovo.predict(X_test)
    print("mean square error is {}".format(mean_squared_error(y_test, svm_predictions)))
    # model accuracy for X_test
    accuracy = svm_model_linear_ovo.score(X_test, y_test)
    print('One VS One SVM accuracy Kernal: ' + str(accuracy))
