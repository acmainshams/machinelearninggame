import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn import preprocessing
from sklearn.metrics import r2_score

from preprocessing import *
from build_data_set import *
from Visualization import *
import FileOperations as fop
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler
import statistics
from statistics import mode
def most_common(List):
    return (mode(List))
def convert_age_rating(C):
    G = []
    for i in C:
     if('+'in i ):
        i = i[:-1]
        i = (str)(i)
        G.append(i)
     else:
         v=most_common(C)
         G.append(v[:-1])

    return G
def ageRating_(Data):
    for i in range(len( Data['Age Rating'])):
        Data["Age Rating"][i]=str(Data["Age Rating"][i]).strip()
    MostFrequentValue = most_common(Data['Age Rating'])# Fill Most Freq
    Data["Age Rating"].replace(np.NAN, MostFrequentValue, inplace=True)
    Data["Age Rating"] = convert_age_rating(Data["Age Rating"])  # Remove the (+) from Age Rating
    return  Data

def Rate(Data, val):
    # Preproceing on rate and strip()
    if (val == "1"):
        y = []
        High = 0
        Low = 0
        Immedirate = 0
        Data["Rate"].replace(np.NAN, '-1', inplace=True)  # to remove it again

        for i in range(len(Data['Rate'])):
            if ( Data["Rate"][i].strip() == 'High'):
                High = High + 1
            elif (Data["Rate"][i].strip() == 'Intermediate'):
                Immedirate = Immedirate + 1
            elif (Data["Rate"][i].strip() == 'Low'):
                Low = Low + 1
        Maxx = max(High, Immedirate, Low)
        if (Maxx == High):
            Data["Rate"].replace('-1', 'High', inplace=True)
        elif (Maxx == Low):
            Data["Rate"].replace('-1', 'Low', inplace=True)
        elif (Maxx == Immedirate):
            Data["Rate"].replace('-1', 'Intermediate', inplace=True)

    return Data

