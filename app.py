import numpy as np
from flask import Flask, request, jsonify, render_template
import pickle
import cv2
from skimage import  feature
import pandas as pd


app = Flask(__name__)
model = pickle.load(open('Knn.pkl', 'rb'))

@app.route('/')
def home():
    return render_template('index.html')
@app.route('/predict',methods=['POST'])
def predict():

    #int_features = [x for x in request.form.values()]
    #print(int_features)
    #preprocessing(int_features)

    Extraction = [3553, 2.99, 4, 15853568, 3245, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0,
                  0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1,
                  0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                  1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    prediction = model.predict(Extraction)# list of features
    print(prediction)
    if(prediction==0) :
        label="High"

    if(prediction==1) :
        label="Intermediate"

    if (prediction == 2):
        label = "Low"

    return render_template('index.html', prediction_text='The Result of Classification is  $ {}'+str("jjjjjjjjjjjj"))

if __name__ == "__main__":
    app.run(debug=True)
