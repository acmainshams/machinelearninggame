import numpy as np
from datetime import datetime

import sklearn
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder ,OneHotEncoder
import pandas as pd
from ColumnPreprocessing import ageRating_, Rate, most_common
from scipy import stats as s
def normalization(DataName , Data):
    Max=Data[DataName].max()
    Min=Data[DataName].min()
    Variance=Max-Min
    Data[DataName]=(Data[DataName]-Min)/Variance
    return  Data

def full_normalization(Data):
    for (col_name , _) in Data.iteritems():
        if col_name=='Average User Rating':
            continue
        Data = normalization(col_name , Data)
    return Data

"""
    we will bring price using regression 
"""
def drop_cols(Data):# Used in Milestone 2
    droped_cols = ["ID" ,"URL" ,  "Icon URL"  , "Original Release Date"  , 'Description',
                   "Current Version Release Date"  , "Subtitle","Name" , 'In-app Purchases',"Developer"]
    Data = Data.drop(droped_cols , axis=1 , inplace=True)

def drop_rows(Data):

    # we need test it
    l = ["Languages"]
    for tmp in l:
        data = Data[ Data[tmp].notnull() ]
    return  data

def replace_NaNs(Data,val):# used in Milestone 2
    if val=="0":
     Data["Average User Rating"].replace(np.NaN, Data["Average User Rating"].mean(),inplace=True)  # replace with average

    Data["User Rating Count"].replace(np.NaN, Data["User Rating Count"].mean(),inplace=True)  # Replaced with average
    Data["Size"].replace(np.NaN, Data["Size"].mean(),inplace=True)  # Replaced with average
    Data["Price"].replace(np.NaN, Data["Price"].mean(),inplace=True)  # Replaced with average
    Data["Languages"].replace(np.NAN,'0',inplace=True)# to remove it again
    Data["Primary Genre"].replace(np.NAN,'0',inplace=True)# to remove it again
    Data["Genres"].replace(np.NAN,'0',inplace=True)# to remove it again
    Data["Original Release Date"].replace(np.NAN,'2/2/2008',inplace=True)# to remove it again
    Data["Current Version Release Date"].replace(np.NAN,'2/2/2019',inplace=True)# to remove it again

def convert_date(str):
    """
    :param str:  single data in string format
    :return:  list of 3 values (day , month , year)
    """
    l = str.split('/')
    l = np.array(l , dtype=np.int)
    return l

def subtract_dates(l1 , l2):
    # year , month , day
    org = datetime(l1[2] , l1[1] , l1[0])
    cur = datetime(l2[2] , l2[1] , l2[0])
    diff = cur-org
    return diff.days
def get_diff_days(d1 , d2):
    """
     more days mean more support , more popularity and successes
    :param d1:  orig data  list  as strings
    :param d2:  cur date list as strings
    :return: list of diff in days as ints
    """

    assert (d1.shape == d2.shape)

    orig = []
    cur = []
    diff_days = []
    # convert the dates
    for i in range(0 , d1.shape[0]):
        orig_date = convert_date(d1[i])
        cur_date  = convert_date(d2[i])
        orig.append(orig_date)
        cur.append(cur_date)
    assert (len(orig)==len(cur))
    # get the difference in days
    for i in range(0 , len(orig)):
        diff_days.append(subtract_dates(orig[i] , cur[i]))
    return diff_days  #actual days



def PrimaryDataset(Data):
 list=[]
 for i in range(len(Data["Primary Genre"])):
  Data["Primary Genre"][i]= Data["Primary Genre"][i].replace(" ", "")
  if Data["Primary Genre"][i] !='0':
        list.append(Data["Primary Genre"][i])
 mostFrequent = most_common(list)
 Data["Primary Genre"].replace('0', mostFrequent, inplace=True)
 DataFrame2=pd.get_dummies(Data['Primary Genre'])
 return DataFrame2

def GenresDataset(Data):
 for i in range (len(Data['Genres'])):
   DataGenres= Data['Genres'][i].replace(" ", "")#strip
   Data['Genres'][i]=DataGenres
   #print(DataGenres)
 SetGenres =[]
 for i in Data['Genres']:
  if i!='0':
   values = i.split(',')
   for k in values:
     SetGenres.append(k)
     #print(k)
 mostFrequent=most_element(SetGenres)
 Data["Genres"].replace('0', mostFrequent, inplace=True)
 DataGenres = pd.Series(Data['Genres']).str.get_dummies(sep=',')
 return DataGenres

 ####
def lanaguageDataset(Data):#Done
 for i in range (len(Data['Languages'])):
   DataLanguages= Data['Languages'][i].replace(" ", "")#trim
   Data['Languages'][i]=DataLanguages
 SetLanguage =[]
 for i in Data['Languages']:
  if i!='0':
   values = i.split(',')
   for k in values:
     SetLanguage.append(k)
 mostFrequent=most_common(SetLanguage)
 Data["Languages"].replace('0', mostFrequent, inplace=True)
 DataLanguages = pd.Series(Data['Languages']).str.get_dummies(sep=',')
 return DataLanguages


def prepare_Data(Data , val,save=False , name="newfile",): # Call all pre-Proceesing
    replace_NaNs(Data,val)#drop columns
    Data=ageRating_(Data)
    Data["game_age"] = get_diff_days(Data['Original Release Date'].values,Data['Current Version Release Date'].values)
    drop_cols(Data)
    # save the file
    if save==True:
        Data.to_csv(name+".csv")
    return Data

def split_data(Data):
    #np.random.seed(1)
    X = Data.iloc[: , 1:]
    Y = Data.iloc[: , 0]

    X_train , X_test , Y_train , Y_test = train_test_split(X , Y , test_size=0.2,shuffle=False,random_state=0)

    return X_train , X_test , Y_train , Y_test

def shuffle(Data):
    np.random.seed(1)
    index = np.random.permutation(Data.index)
    Data = Data.reindex(index)
    return Data

def BuildDataframe(Data,val):

 if val=="0":# if regression
  value=Data['Average User Rating']
 else: # if classifer
     value=Data['Rate']

 DataFrame=pd.DataFrame(value)
 AllData=["User Rating Count","Price","Age Rating","Size","game_age"] # for all
 for i in (AllData):
  DataFrame[i]=Data[i]
 return DataFrame

def most_element(liste):
    numeral=[[liste.count(nb), nb] for nb in liste]
    numeral.sort(key=lambda x:x[0], reverse=True)
    return(numeral[0][1])
import  pickle
from sklearn.metrics import mean_squared_error
def FinalTest(filename,d,AlogName): # for loading models
 X_valid = d.iloc[:, 1:]
 Y_valid = d.iloc[:, 0]
 model = pickle.load(open(filename, 'rb'))
 Y_pred=model.predict(X_valid)
 print(AlogName+"mean square error is {}".format(mean_squared_error(Y_valid, Y_pred)))
 if AlogName!="SimpleLinearRegression" and AlogName!="SVMRegression":
  print(AlogName+"accuracy is ", model.score(X_valid, Y_valid))
 else:
     print("R^2 Score : ",sklearn.metrics.r2_score(Y_valid, Y_pred, sample_weight=None, multioutput='uniform_average'))
